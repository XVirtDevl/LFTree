#include "LFTree.hpp"
#include<iostream>
#include <unistd.h>




typedef lft::eventserver::NewEvent<0, int> PrintInt;
typedef lft::eventserver::NewEvent<1,char*> PrintChar;
typedef lft::Tuple<PrintInt,PrintChar> TupelEvent;

class PrintInteger : public PrintInt::EventReceiver
{
	public:
		void Receive( int x )
		{
			std::cout<<"Printing integer: "<<x<<std::endl;
		}
};

class PrintString : public PrintChar::EventReceiver
{
	public:
		void Receive( char *str )
		{
			std::cout<<"Printing string: "<<str<<std::endl;
		}
};




int main()
{
	lft::eventserver::EventServer<TupelEvent> evS;
	evS.Initialise(4);

	PrintInteger y;
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);
	evS.RegisterForEvent<PrintInt::UniqueEventNum>(&y);

	
	PrintInt::Event ev(123);
	
	evS.TriggerEventAsync<PrintInt::UniqueEventNum>(&ev);

	return 0;
}
