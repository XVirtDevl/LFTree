#ifndef _LOCK_FREE_TREE_TUPLE_HPP_
#define _LOCK_FREE_TREE_TUPLE_HPP_

#include "meta.hpp"


namespace lft
{

	template<typename ...AllArguments>
	struct Tuple
	{
		static_assert( always_false<AllArguments...>::value, "A Tuple with zero members does not contain any space!");
	};


	
	template<int N,typename Head, typename ...Tail>
	struct SelectTuple : SelectTuple<N-1,Tail...>
	{};
	template<typename Head, typename ...Tail>
	struct SelectTuple<0,Head,Tail...>
	{
		using ContainedType = Head;
		using value = Tuple<Head,Tail...>;
	};


	template<typename T, typename ...Args>
	struct Tuple<T,Args...> : Tuple<Args...>
	{
			constexpr static unsigned long containedElem = variadic_argument_count<T,Args...>::value;

			T _val;

			Tuple(T newVal, Args ...arguments) : Tuple<Args...>::Tuple(arguments...)
			{
				_val = newVal;
			}

			template<int N>typename SelectTuple<N,T,Args...>::ContainedType get() const
			{
				return SelectTuple<N,T,Args...>::value::_val;
			}

			template<int N>void set( typename SelectTuple<N,T,Args...>::ContainedType newVal)
			{
				SelectTuple<N,T,Args...>::value::_val = newVal;		
			}

	};

	template<typename T>
	struct Tuple<T>
	{
		T _val;
		Tuple( T value)
		{
			_val = value;
		}	
		
		template<int N>T get() const
		{
			return _val;
		}
	};	

	
	template< int NElem >
	struct UnrollThings
	{
		template<typename ClassName, typename ...ClassFunctionArguments, typename ...TupleElements, typename ...UnrolledArgs>
		static void Unroll( ClassName *obj, 
					void (ClassName::*cfunc)(ClassFunctionArguments...),
					const Tuple<TupleElements...>& t, 
					UnrolledArgs ...args)
		{
			UnrollThings<NElem-1>::Unroll(obj,cfunc,t,t.template get<(NElem-1)>(),args...);
		}
	};

	template<>
	struct UnrollThings<0>
	{
		template<typename ClassName, typename ...ClassFunctionArguments, typename ...TupleElements, typename ...UnrolledArgs>
		static void Unroll( ClassName *obj, void (ClassName::*cfunc)(ClassFunctionArguments...),const Tuple<TupleElements...>& tup, UnrolledArgs ...args)
		{
			(obj->*cfunc)(args...);	
		}
	};




	template<int N, typename Head, typename ...Tail>struct SelectTupleX;
	template<int N, typename Head, typename ...Tail>struct SelectTupleX<N,Tuple<Head,Tail...>> : SelectTupleX<N-1,Tuple<Tail...>>
	{
	};
	template<typename Head, typename ...Tail>struct SelectTupleX<0,Tuple<Head,Tail...>>
	{
		using ContainedType = Head;
	};
};













#endif
