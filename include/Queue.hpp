#ifndef _LOCK_FREE_TREE_QUEUE_HPP_
#define _LOCK_FREE_TREE_QUEUE_HPP_

#include "meta.hpp"
#include "ia32intrin.h"
#include <thread>
#include "container.hpp"

namespace lft
{
	template<typename ContainedType, int NMaxSize,ContainedType invVal=nullptr>class UnoptimizedQueue
	{
		static_assert(is_pointer<ContainedType>::value, "The from Queue conatained Type must be a pointer!");
		
		private:
			using OperType =  unsigned long;	

			OperType _rdoffset;
			OperType _wroffset;

			volatile ContainedType _arr[NMaxSize];


			template<typename ...Arguments>
			void InternPush( OperType off, ContainedType val, Arguments... args)
			{
				if( off == NMaxSize )
					off = 0;

				_arr[off] = val;

				InternPush(off+1,args...);
			}
			
			

			void InternPush(OperType off, ContainedType val)
			{
				if( off == NMaxSize )
					off = 0;

				_arr[off] = val;
			}
		public:
			UnoptimizedQueue():_rdoffset(0),_wroffset(0)
			{
				for( int i = 0; i < NMaxSize; i++ )
					_arr[i] = invVal;
			}

			template<typename ...Args>
			void push(ContainedType val, Args... arguments)
			{	
				OperType workIndex;
				for(;;)
				{
					workIndex = _InterlockedExchangeAdd((void*)&_wroffset, variadic_argument_count<int,Args...>::value);
					if( workIndex < NMaxSize )
						break;

					_wroffset = 0;
				}

				InternPush(workIndex, val,arguments...);
			}
			
			void push(ContainedType val)
			{
				OperType workIndex;
				
				for(;;)
				{
					workIndex = _InterlockedExchangeAdd((void*)&_wroffset,1);
					if(workIndex < NMaxSize)
						break;
					_wroffset = 0;
				}
				
				_arr[workIndex] = val;
			}
			
			ContainedType popw()
			{
				OperType workIndex;	
				ContainedType tmp;

				for(;;)
				{
					workIndex = _InterlockedExchangeAdd((void*)&_rdoffset,1);
					if(workIndex < NMaxSize)
						break;

					_rdoffset = 0;
				}
			
				for( tmp = _arr[workIndex]; tmp == invVal; tmp = _arr[workIndex])	
				{ std::this_thread::yield(); }
				
				_arr[workIndex] = invVal;
				return tmp;
			}
			
	};
	
	template<int N,typename T>inline unsigned long LFTInterlockedExchangeAdd(void *addr, T x)
	{ return _InterlockedExchangeAdd( addr, x); }
	template<>inline unsigned long LFTInterlockedExchangeAdd<1,unsigned char>(void *addr, unsigned char x)
	{ return _InterlockedExchangeAdd8( addr, x); }
	template<>inline unsigned long LFTInterlockedExchangeAdd<2,unsigned short>(void *addr, unsigned short x)
	{ return _InterlockedExchangeAdd16(addr, x); }



	template<typename ContainedType, int NMaxSize, ContainedType invVal=nullptr>class OptimizedQueue
	{
		static_assert(is_pointer<ContainedType>::value, "The from Queue conatained Type must be a pointer!");
		static_assert((NMaxSize==256)||(NMaxSize==65565), "The optimized queue must have 256 or 65565 entrys to work out!");	
	
		private:
			using OperType = typename SelectType<NMaxSize==256,unsigned char, unsigned short>::Type;
			static constexpr unsigned long OperSize = (NMaxSize==256)?1:2;	

			OperType _rdoffset;
			OperType _wroffset;

			volatile ContainedType _arr[NMaxSize];


			template<typename ...Arguments>
			void InternPush( OperType off, ContainedType val, Arguments... args)
			{	
				_arr[off] = val;
				InternPush(off+1,args...);
			}
			
			

			void InternPush(OperType off, ContainedType val)
			{	
				_arr[off] = val;
			}
		public:
			OptimizedQueue():_rdoffset(0),_wroffset(0)
			{
				for( int i = 0; i < NMaxSize; i++ )
					_arr[i] = invVal;
			}

			template<typename ...Args>
			void push(ContainedType val, Args... arguments)
			{	
				OperType workIndex  = LFTInterlockedExchangeAdd<OperSize>((void*)&_wroffset, variadic_argument_count<int,Args...>::value);	
				InternPush(workIndex, val,arguments...);
			}
		
			void push( const unsigned long sz, ContainedType *_valArr )
			{
				OperType workIndex = LFTInterlockedExchangeAdd<OperSize>((void*)&_wroffset, sz);
				for( unsigned long i = 0; i < sz; i++)
				{ 
					_arr[workIndex] = _valArr[i]; 
					workIndex++;
				}
			}

			void push( container<ContainedType> &x )
			{
				OperType workIndex = LFTInterlockedExchangeAdd<OperSize>((void*)&_wroffset, x.size());
				for( unsigned long i = 0; i < x.size(); i++)
				{ 
					_arr[workIndex] = x[i]; 
					workIndex++;
				}
			}
	
			void push(ContainedType val)
			{
				OperType workIndex = LFTInterlockedExchangeAdd<OperSize>((void*)&_wroffset,1);
				_arr[workIndex] = val;
			}
			
			ContainedType popw()
			{
				OperType workIndex;	
				ContainedType tmp;
	
				workIndex = LFTInterlockedExchangeAdd<OperSize>((void*)&_rdoffset,1);
		
				for( tmp = _arr[workIndex]; tmp == invVal; tmp = _arr[workIndex])	
				{ std::this_thread::yield(); }
				
				_arr[workIndex] = invVal;
				return tmp;
			}
	};



	template<typename ContainedType, int NMaxSize, ContainedType invVal=nullptr>class Queue : public SelectType<NMaxSize==256,OptimizedQueue<ContainedType,NMaxSize,invVal>,
																  typename SelectType<NMaxSize==65565,
																   OptimizedQueue<ContainedType,NMaxSize,invVal>,
																   UnoptimizedQueue<ContainedType,NMaxSize,invVal>>::Type>::Type
	{
		public:
			struct MultiplePush
			{
				ContainedType *_valArr;
				unsigned long _num_entrys;
				MultiplePush( unsigned long sz, void *addr ) : _num_entrys(sz),_valArr((ContainedType*)addr){}
			};
		
			void push_q(MultiplePush &p)
			{
				push(p._num_entrys, p._valArr);
			}
	};
};

#define STACK_ALLOC(size) _alloca(size)


#endif
