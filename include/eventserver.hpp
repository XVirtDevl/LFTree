#ifndef _LOCK_FREE_TREE_EVENT_SERVER_HPP_
#define _LOCK_FREE_TREE_EVENT_SERVER_HPP_
#include "tupel.hpp"
#include "Queue.hpp"
#include <atomic>

namespace lft
{
	
namespace eventserver
{
	class GeneralReceiver
	{
	};

	class GeneralEvent
	{
		public:
			virtual void Handle( GeneralReceiver *rec ) = 0;
	};
	

	template<int NWhichOrder,typename ...Arguments>class NewEvent
	{
		public:
			static constexpr unsigned long UniqueEventNum = NWhichOrder;
	
			class EventReceiver : public GeneralReceiver 
			{
				public:
					virtual void Receive(Arguments ...args) = 0;
			};
				
			class Event : public GeneralEvent
			{
				private:
					Tuple<Arguments...> funcArgs;
					std::atomic<unsigned long> _estimatedFinishes;
				public:
					Event(Arguments ...args) : funcArgs(args...)
					{}
				
					void SetEstimatedWorkCount( unsigned long x )
					{
						_estimatedFinishes = x;
					}

				

					void SyncWithThreads()
					{
						for(;;)
							if( _estimatedFinishes.load() == 0 )
								break;
					}
				
					void Handle(GeneralReceiver *rec) override
					{
						UnrollThings< variadic_argument_count<Arguments...>::value >::Unroll((EventReceiver*)rec,&EventReceiver::Receive,funcArgs);
						_estimatedFinishes.fetch_sub(1);
					}
			};
	};



	template<int N, typename ...Arguments>struct TupleTotallyOrdered
	{
		static constexpr bool value = (SelectTuple<N,Arguments...>::ContainedType::UniqueEventNum==N)?TupleTotallyOrdered<N-1,Arguments...>::value:false;
	};
	template<typename ...Arguments>struct TupleTotallyOrdered<0,Arguments...>
	{
		static constexpr bool value = (SelectTuple<0,Arguments...>::ContainedType::UniqueEventNum==0)?true:false;
	};

	template<typename ...Arguments>struct CheckTotallyOrdered;
	template<typename ...Arguments>struct CheckTotallyOrdered<Tuple<Arguments...>>
	{
		constexpr static bool value = TupleTotallyOrdered<variadic_argument_count<Arguments...>::value-1,Arguments...>::value;
	};
	





	class Command
	{
		private:
			GeneralReceiver *_rec;
			GeneralEvent *_ev;
		public:
			Command( GeneralReceiver *rec, GeneralEvent *ev) : _rec(rec), _ev(ev){}
			void Execute()
			{
				_ev->Handle(_rec);
			}		
	};

	template<typename T,typename ...Arguments>
	class EventServer;

	template<typename ...Arguments>
	static void ThreadStartPoint( EventServer<Tuple<Arguments...>> *evServer )
	{
		evServer->ThreadEntryPoint();
	}

	template<typename ...Arguments>
	class EventServer<Tuple<Arguments...>>
	{
		static_assert( CheckTotallyOrdered<Tuple<Arguments...>>::value, "Static assert triggered, the input tuple must be totally ordered" );
		static constexpr unsigned long numberOfEvents = Tuple<Arguments...>::containedElem;

		private:
			container<GeneralReceiver*> _eventNotifierChain[ numberOfEvents+1 ];
			Queue<Command*, 256> _commandQeue;
			std::thread *_threads;
			unsigned long _numThreads;		
	
			typedef NewEvent<1,int> ShutdownEvent;

			class ShutdownClass : public ShutdownEvent::EventReceiver
			{
				public:
					void Receive(int i)
					{
						throw i;
					}
			};

		public:
			
			EventServer() :_threads(nullptr)
			{
			}


			void Initialise(unsigned long numThreads)
			{	
				_threads = new std::thread[numThreads];
				
				for( unsigned long i = 0; i < numThreads; i++ )
				{
						_eventNotifierChain[ numberOfEvents ].insert(new ShutdownClass());	
						_threads[i] = std::move( std::thread( ThreadStartPoint<Arguments...>, this ) );
				}
				_numThreads = numThreads;
			}


			void ThreadEntryPoint()
			{
				Command *x;
				try
				{
					for(;;)
					{
						x = _commandQeue.popw();
						x->Execute();
						delete x;
					}
				}
				catch( int i )
				{
					delete x;
				}
			}

			template<int N>void RegisterForEvent( typename SelectTupleX<N,Tuple<Arguments...>>::ContainedType::EventReceiver *eveRec )
			{
				_eventNotifierChain[N].insert(eveRec);	
			}

			template<int N>void UnregisterFromEvent( typename SelectTupleX<N,Tuple<Arguments...>>::ContainedType::EventReceiver *eveRec)
			{
				for( unsigned long i = 0; i < _eventNotifierChain[N].size(); i++)
				{
					if( _eventNotifierChain[N][i] == eveRec )
					{
						_eventNotifierChain[N].erase(i);
						break;
					}
				}
			}
			

			template<int N>void TriggerEventSync( typename SelectTupleX<N,Tuple<Arguments...>>::ContainedType::Event *eventTo)
			{
				for( unsigned long i = 0; i < _eventNotifierChain[N].size(); i++ )
					Command(_eventNotifierChain[N][i],eventTo).Execute();
			}

			template<int N>void TriggerEventAsync( typename SelectTupleX<N,Tuple<Arguments...>>::ContainedType::Event *eventTo )
			{
				unsigned long xsize = _eventNotifierChain[N].size();
				Queue<Command*,256>::MultiplePush mp(xsize, STACK_ALLOC(xsize*sizeof(Command*)));
				
				for( unsigned long i = 0; i < xsize; i++ )
					mp._valArr[i] = new Command(_eventNotifierChain[N][i], eventTo);
			
				_commandQeue.push_q(mp);
			}

			~EventServer()
			{
				unsigned long xsize = _eventNotifierChain[numberOfEvents].size();
				Queue<Command*,256>::MultiplePush mp(xsize, STACK_ALLOC(xsize*sizeof(Command*)));
			
				ShutdownEvent::Event eve(-1);	
				for( unsigned long i = 0; i < xsize; i++ )
					mp._valArr[i] = new Command(_eventNotifierChain[numberOfEvents][i], &eve);
	
				_commandQeue.push_q(mp);
			
				for( int i = 0; i < _numThreads; i++ )
					_threads[i].join();
	
				if( _threads != nullptr )
				{
					delete []_threads;
					_threads = nullptr;
				}
			}
		
	};






};
};





#endif
