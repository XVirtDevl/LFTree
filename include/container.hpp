#ifndef _LOCK_FREE_TREE_CONTAINER_HPP_
#define _LOCK_FREE_TREE_CONTAINER_HPP_

#include "meta.hpp"
#include "ia32intrin.h"
#include <cstring>


namespace lft
{
	template<typename ContType>class container
	{
		private:
			ContType *_valArr;
			volatile unsigned long _nextWrite;
			volatile unsigned long _size;
			volatile unsigned long _maxSize;
		
			void IncreaseCapacity()
			{
				unsigned long oldWr;
				for(;;)
				{
					oldWr = _InterlockedExchangeAdd((void*) &_nextWrite, _maxSize*2+1000);
					if( oldWr < (_maxSize+999))
						break;
				}		
	
				ContType *nArr = new ContType[ _maxSize*2 ];
				memcpy( nArr, _valArr, _maxSize*sizeof(ContType));
				delete []_valArr;
				_valArr = nArr;
				_maxSize*=2;
				_nextWrite = oldWr;
			}

		public:
			container(unsigned long sz=100):_maxSize(sz),_valArr(new ContType[sz]),_nextWrite(0),_size(0)
			{}
					
			~container()
			{
				if( _valArr )
				{
					delete []_valArr;	
					_valArr = nullptr;
				}
				_nextWrite = _size = _maxSize = 0;
			}	
		
			void insert( ContType val )
			{
				unsigned long wrt;
				for(;;)
				{
					wrt = _InterlockedExchangeAdd((void*)&_nextWrite, 1);

					if( wrt < _maxSize )
						break;
					else if( wrt == _maxSize )
						IncreaseCapacity();
				}
					

				_valArr[wrt] = val;
				__asm__ volatile("addl $1, %0":"+m"(_size));
			}

			unsigned long size()
			{
				return _size;
			}

			void erase( unsigned long index )
			{
				if( index >= _size )
					throw 0;

				unsigned long oldWr;
				for(;;)
				{
					oldWr = _InterlockedExchangeAdd( (void*)&_nextWrite, _maxSize+1000 );
					
					if( oldWr <_maxSize )
						break;
				}

			
				memcpy( &_valArr[index], &_valArr[index+1], (_size-(index+1))*sizeof(ContType));
				__asm__ volatile("subl $1, %0":"+m"(_size));
				_nextWrite = oldWr;
			}

			ContType operator[]( unsigned long elem ) const
			{
				if( elem >= _size )
					throw 0;
				return _valArr[elem];
			}
			
	};
};



#endif
