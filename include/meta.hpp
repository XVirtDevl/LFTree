#ifndef _LOCK_FREE_TREE_META_HPP_
#define _LOCK_FREE_TREE_META_HPP_

namespace lft
{
	template<typename T>struct is_pointer
	{
		static constexpr bool value = false;
	};
	
	template<typename T>struct is_pointer<T*>
	{
		static constexpr bool value = true;
	};

	template<typename ...T>struct always_false
	{
		static constexpr bool value = false;
	};

	template<typename ...T>struct always_true
	{
		static constexpr bool value  = true;
	};

	template<typename T, typename ...Arguments>struct variadic_argument_count
	{
		static constexpr unsigned long value = 1+variadic_argument_count<Arguments...>::value;
	};

	template<typename T>struct variadic_argument_count<T>
	{
		static constexpr unsigned long value=1;
	};

	template<bool C, typename T, typename F>struct SelectType
	{
		using Type = T;
	};
	template<typename T, typename F>struct SelectType<false, T, F>
	{
		using Type = F;
	};
};



#endif
